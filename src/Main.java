import java.net.MalformedURLException;
import java.net.URL;

import stopwatch.StopWatch;

/**
 * @author Nattapat Sukpootanan.
 * Main for counting word and syllables from http://se.cpe.ku.ac.th/dictionary.txt.
 * */
public class Main {
	/**
	 * Main for runnning.
	 * @param agrs is not used.
	 * */
	public static void main(String[] args) throws MalformedURLException {
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = new URL( DICT_URL );
		StopWatch stopwatch = new StopWatch();
		WordCounter counter = new WordCounter();
		stopwatch.start();
		int wordcount = counter.countWords( url );
		int syllables = counter.getSyllableCount( );
		stopwatch.stop();
		System.out.println("Reading words from http://se.cpe.ku.ac.th/dictionary");
		System.out.println(String.format("Counted %d syllables in %d words",syllables,wordcount));
		System.out.println(String.format("Elapsed time: %.3f sec", stopwatch.getElapsed()));
		
		
	}
}
