import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Nattapat Sukpootanan
 * WordCounter for count word and syllables.
 * */
public class WordCounter {
	private int syllables;
	/**
	 * State for use pattern.
	 * */
	public enum State{
		START,CONSONATE,VOWEL,E_FIRST,DASH,NON_WORD;
	}
	/**
	 * Constructor.
	 * */
	public WordCounter(){

	}
	/**
	 * ountSyllables count the Syllables of word
	 *@param word for count Syllables.
	 *@return syllables. 
	 * */
	public int countSyllables( String word ){
		int syllables = 0;
		State state = State.START;
		char [] array = word.toCharArray();
		for(char c: array ){
			if(c=='\'')continue;
			switch(state) {
			case START : 
				if(this.isLetter(c)&&!this.isVowel(c)){state = State.CONSONATE;}
				else if(isE_FIRST(c))state = State.E_FIRST;
				else if(this.isVowel(c)||c=='y'||c=='Y'){ 
					state = State.VOWEL;}
				else if(!this.isLetter(c)) state = State.NON_WORD;
				break;

			case CONSONATE : 
				if(isE_FIRST(c)) state = State.E_FIRST;
				else if(this.isVowel(c)||c=='y'||c=='Y') {state = State.VOWEL;}
				else if(this.isLetter(c)&&!this.isVowel(c)){state = State.CONSONATE;}
				else if(c=='-') state = State.DASH;
				else if(!this.isLetter(c)) state = State.NON_WORD;
				break;

			case VOWEL:
				if(this.isLetter(c)&&!this.isVowel(c)){
					syllables++;
					state = State.CONSONATE;
				}
				else if(this.isVowel(c)){ 
					state = State.VOWEL;}
				else if(c=='-'){
					syllables++;
					System.out.print(3);
					state = State.DASH;
				}
				else if(!this.isLetter(c)){ 
					state = State.NON_WORD;
				}
				else {
					return syllables++;
				}
				break;

			case E_FIRST:
				if(this.isLetter(c)&&!this.isVowel(c)){
					syllables++;
					state = State.CONSONATE;
				}
				else if(this.isVowel(c)) state = State.VOWEL;
				else if(c=='-'){
					state = State.DASH;
				}
				else if(!this.isLetter(c)){ 
					state = State.NON_WORD;
				}
				else if(syllables==0) return syllables++;
				else return syllables;
				break;
			case DASH :
				if(this.isLetter(c)&&!this.isVowel(c)){state = State.CONSONATE;}
				else if(isE_FIRST(c))state = State.E_FIRST;
				else if(this.isVowel(c)||c=='y'||c=='Y') state = State.VOWEL;
				else state = State.NON_WORD;

				break; 
			case NON_WORD :
				syllables = 0;
				break;
			}
		}
		if(state==State.VOWEL||state==State.E_FIRST&&syllables==0) syllables++;
		else if(state==State.DASH) return 0; 
		return syllables;
	}
	/**
	 * Checking it is Vowel.
	 * @param c is Character for checking.
	 * @return true if it is Vowel,false if it is not Vowel.
	 * */
	private boolean isVowel(char c){
		return "AEIOUaeiou".indexOf(c)>=0;
	}
	/**
	 * checking in state E_FIRST is E or e.
	 * @param c is Character for checking.
	 * @return true is it is chracter e or E.
	 * */
	private boolean isE_FIRST(char c){
		return c=='E'||c=='e';
	}
	/**
	 *checking character that is Letter or not.
	 *@param c is Chracter for checking
	 *@return true if it is Letter , false if it is not.
	 * */
	private boolean isLetter(char c){
		return Character.isLetter(c);
	}
	/**
	 * Count all words read from an input stream an return the total count.
	 * @param instream is the input stream to read from.
	 * @return number of words in the InputStream. -1 if the stream
	 * cannot be read.
	 */
	public int countWords(InputStream instream) {
		BufferedReader breader = new BufferedReader(new InputStreamReader(instream));
	    int word = 0;
	    while( true ) {
	    	 String line="";
			try {
				line = breader.readLine( );
			} catch (IOException e) {
				e.printStackTrace();
			}
	    	 if (line == null) break;
	    	 StringTokenizer token = new StringTokenizer(line);
	    	 while(token.hasMoreTokens()){
	    		 word++;
	 	    	this.syllables += this.countSyllables(token.nextToken());
	    	 }
	    }
		return word;
	}
	/**
	 *count Words in URL.
	 *@param url is address that want to counting words.s
	 *@return number of words
	 */
	public int countWords(URL url) {
		InputStream input = null;
		try {
			input = url.openStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return countWords(input);
	}
	/**
	 * getAll Syllables that counted.
	 * @return number all of Syllables. 
	 * */
	public int getSyllableCount() {
		
		return this.syllables;
	}

}
