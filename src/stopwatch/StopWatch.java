package stopwatch;
/**
 * A StopWatch that measures elapsed time between a starting time
 * and stopping time, or until the present time.
 * @author Nattapat Sukpootanan
 * @version 1.0
￼*/
public class StopWatch {
	/** constant for converting nanoseconds to seconds. */
	private static final double NANOSECONDS = 1.0E-9;
	/** time that the stopwatch was started, in nanoseconds. */
	private double startTime;
	private double stopTime;
	public StopWatch(){
		startTime = 0;
		stopTime = 0;
	}
	/**
	 * getelasped time in sec.
	 * @return elapsed time 
	 */
	public double getElapsed(){
		if(isRunning())
			return (System.nanoTime( )-this.startTime )*StopWatch.NANOSECONDS;
		else { 
			return(this.stopTime-this.startTime )*StopWatch.NANOSECONDS;}
	}
	/**
	 * @return true if StopWatch is running , false if it doesn't run
	 * */
	public boolean isRunning(){
		if(stopTime==0){
			return true;
		}
		else return false;
	}
	/**
	 * start the watch
	 */
	public void start(){
		startTime = System.nanoTime( );
		if(!isRunning()){
			this.startTime=0;
			this.stopTime = 0;
		}
	}
	/**
	 * stop the watch
	 */
	public void stop(){
		this.stopTime = System.nanoTime();

	}
}